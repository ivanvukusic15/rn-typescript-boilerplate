import { Navigation } from 'react-native-navigation';
import Home from '../View/Home';
import Login from '../View/Login';
import Weather from '../View/Weather';

export default (store, Provider) => {
  Navigation.registerComponent('RNTypescriptBoilerplate.Home', () => Home, store, Provider);
  Navigation.registerComponent('RNTypescriptBoilerplate.Login', () => Login, store, Provider);
  Navigation.registerComponent('RNTypescriptBoilerplate.Weather', () => Weather, store, Provider);
};
