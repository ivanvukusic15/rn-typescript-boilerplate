import { Component } from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import registerScreens from './Navigators/index';
import { store } from './Store';
import { NavigatorActions } from './Actions';
import { fontFamily } from './Style';

registerScreens(store, Provider);

export default class App extends Component {
  constructor(props) {
    super(props);
    store.subscribe(this.onStoreUpdate.bind(this));
    store.dispatch(NavigatorActions.initializeNavigator('initial'));
  }

  onStoreUpdate() {
    const { page } = store.getState().navigator;
    if (page === 'initial') {
      this.startApp();
    }
  }

  startApp() {
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'RNTypescriptBoilerplate.Home',
        title: 'Welcome',
      },
      appStyle: {
        keepStyleAcrossPush: false,
        navBarBackgroundColor: '#353535',
        navBarTextColor: '#EEE',
        navBarTextFontFamily: fontFamily,
        navBarButtonColor: '#DDD',
        statusBarTextColorScheme: 'light',
      },
    });
    return;
  }
}
