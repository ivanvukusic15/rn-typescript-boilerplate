import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { Button, Text } from '../../Common';
import { styles } from './login.style';

export default class Login extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  public render(): JSX.Element {
    return (
      <View style={styles.container}>
        <Text style={styles.login}>Login</Text>
        <View>
          <Button onPress={() => this.props.navigator.pop()}>LOGIN</Button>
        </View>
        <Text />
      </View>
    );
  }
}

export interface IState {}
export interface IProps {
  navigator: INavigation;
}
interface INavigation {
  pop: Function;
}
