import React from 'react';
import { shallow } from 'enzyme';
import Login from './';

describe('Testing Login component', () => {
  let component;
  beforeEach(() => {
    component = shallow(<Login navigation={null} />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
