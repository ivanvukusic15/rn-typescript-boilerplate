import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { Text } from '../../Common';
import { connect } from 'react-redux';
import { WeatherActions } from '../../Actions';
import { styles } from './weather.style';

export class Weather extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  public componentDidMount(): void {
    const city = 'London';
    const state = 'uk';
    const appId = 'b6907d289e10d714a6e88b30761fae22';
    const url = `http://samples.openweathermap.org/data/2.5/weather?q=${city},${state}&appid=${appId}`;
    this.props.currentWeatherByCity({ url });
  }

  public render(): JSX.Element {
    const { currentWeather } = this.props;
    if (currentWeather) {
      const { name, main } = currentWeather;
      const { humidity, pressure, temp, temp_max, temp_min } = main;
      return (
        <View style={styles.container}>
          <Text style={styles.title}>{name}</Text>
          <View>
            <Text style={styles.text}>Humidity: {humidity}</Text>
            <Text style={styles.text}>Pressure: {pressure}</Text>
            <Text style={styles.text}>Temp: {temp}</Text>
            <Text style={styles.text}>Min: {temp_min}</Text>
            <Text style={styles.text}>Max: {temp_max}</Text>
          </View>
          <Text />
        </View>
      );
    }
    return null;
  }
}

const mapSateToProps = ({ currentWeather }: IProps) => ({ currentWeather });

export default connect(mapSateToProps, {
  currentWeatherByCity: WeatherActions.currentWeatherByCity,
})(Weather);

export interface IState {}
export interface IProps {
  currentWeather: ICurrentWeather;
  currentWeatherByCity: Function;
}
export interface ICurrentWeather {
  name: string;
  main: IWeatherMain;
}
export interface IWeatherMain {
  humidity: number;
  pressure: number;
  temp: number;
  temp_max: number;
  temp_min: number;
}
