import React from 'react';
import { shallow } from 'enzyme';
import { Weather } from './';
import { WeatherActions } from '../../Actions/WeatherActions';
import { mockWeather } from '../../Utils/Mock/mockWeather';

describe('Testing Weather component', () => {
  let component;
  beforeEach(() => {
    component = shallow(
      <Weather
        currentWeather={mockWeather.weatherByCityResponse}
        currentWeatherByCity={WeatherActions.currentWeatherByCity}
      />,
    );
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
