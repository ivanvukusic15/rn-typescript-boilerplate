import React from 'react';
import { shallow } from 'enzyme';
import { Home } from './';

const navigation = {};

describe('Testing Home component', () => {
  let component;
  beforeEach(() => {
    component = shallow(<Home />);
  });

  it('renders as expected', () => {
    expect(component).toMatchSnapshot();
  });
});
