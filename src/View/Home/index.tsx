import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { Button, Text } from '../../Common';
import { styles } from './home.styles';

import { connect } from 'react-redux';

export class Home extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  private onWeatherPress(): void {
    this.props.navigator.push({
      screen: 'RNTypescriptBoilerplate.Weather',
      title: 'Weather',
      backButtonTitle: '',
    });
  }

  private onLoginPress(): void {
    console.log(this.props.navigator);
    this.props.navigator.push({
      screen: 'RNTypescriptBoilerplate.Login',
      title: 'Login',
      backButtonTitle: '',
    });
  }

  public render(): JSX.Element {
    return (
      <View style={styles.container}>
        <Text style={styles.login}>Boilerplate</Text>
        <View>
          <Button onPress={() => this.onWeatherPress()} testID={'weatherButton'}>
            WEATHER
          </Button>
          <Button onPress={() => this.onLoginPress()} testID={'loginButton'}>
            LOGIN
          </Button>
        </View>
        <Text />
      </View>
    );
  }
}

export default connect()(Home);

export interface IState {}
export interface IProps {
  navigator: INavigation;
}

export interface INavigation {
  push: Function;
}
