import { Platform, StyleSheet } from 'react-native';

export const fontFamily = Platform.OS === 'ios' ? 'Avenir' : 'sans-serif';

export const global: any = StyleSheet.create({
  fontFamily: {
    fontFamily: fontFamily,
  },
});
