import Immutable from 'seamless-immutable';
import { NavigatorActions } from '../../Actions';

const INITIAL_STATE = Immutable({
  page: 'initial',
});

export const NavigatorReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case NavigatorActions.Type.INITIALIZE:
      return { page: action.payload };
    default:
      return { page: 'next' };
  }
};
