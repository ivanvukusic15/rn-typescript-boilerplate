import { INITIAL_STATE } from './';
import { UserReducer } from './';
import { mockUser } from '../../Utils/Mock/mockUser';
import { UserActions } from '../../Actions/UserActions';

describe('USER Reducer', () => {
  it('should return the initial state', () => {
    expect(UserReducer(INITIAL_STATE, { type: 'unexpected' })).toEqual(INITIAL_STATE);
  });

  it('should handle USER_SAVE', () => {
    const successAction = {
      type: UserActions.Type.USER_SAVE,
      payload: mockUser.user,
    };
    expect(UserReducer(mockUser.user, successAction)).toEqual(mockUser.user);
  });
});
