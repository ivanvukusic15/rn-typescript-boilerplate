import { handleActions } from 'redux-actions';

import { UserActions } from '../../Actions/UserActions';

export const INITIAL_STATE = {
  id: '',
  firstName: '',
  lastName: '',
  email: '',
};

export const UserReducer = handleActions(
  {
    [UserActions.Type.USER_SAVE]: (state, action) => {
      if (action.payload) {
        return { ...action.payload };
      } else {
        return state;
      }
    },
  },
  INITIAL_STATE,
);
