import { handleActions } from 'redux-actions';
import { WeatherActions } from '../../Actions/WeatherActions';

export const INITIAL_STATE = {
  name: '',
  main: {
    humidity: 0,
    pressure: 0,
    temp: 0,
    temp_max: 0,
    temp_min: 0,
  },
};

export const WeatherReducer = handleActions(
  {
    [WeatherActions.Type.WEATHER_UPDATE_SUCCEEDED]: (state, action) => {
      if (action.payload && action.payload) {
        return { ...action.payload };
      } else {
        return state;
      }
    },
  },
  INITIAL_STATE,
);
