import { INITIAL_STATE } from './';
import { WeatherReducer } from './';
import { mockWeather } from '../../Utils/Mock/mockWeather';
import { WeatherActions } from '../../Actions';

describe('WEATHER Reducer', () => {
  it('should return the initial state', () => {
    expect(WeatherReducer(INITIAL_STATE, { type: 'unexpected' })).toEqual(INITIAL_STATE);
  });

  it('should handle WEATHER_UPDATE', () => {
    const successAction = {
      type: WeatherActions.Type.WEATHER_UPDATE_SUCCEEDED,
      payload: mockWeather.weatherByCityResponse,
    };
    expect(WeatherReducer(mockWeather.weatherByCityResponse, successAction)).toEqual(mockWeather.weatherByCityResponse);
  });
});
