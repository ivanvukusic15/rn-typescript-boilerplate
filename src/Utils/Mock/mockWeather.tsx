export const mockWeather = {
  weatherByCity: {
    url: 'http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22',
    test: true,
  },
  weatherByCityResponse: {
    name: 'London',
    main: {
      humidity: 81,
      pressure: 1012,
      temp: 280,
      temp_max: 12,
      temp_min: 19,
    },
  },
};
