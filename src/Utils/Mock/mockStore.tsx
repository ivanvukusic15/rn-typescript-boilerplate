import configureMockStore from 'redux-mock-store';
import ReduxThunk from 'redux-thunk';

const middlewares = [ReduxThunk];
export const mockStore = configureMockStore(middlewares);
