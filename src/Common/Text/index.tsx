import React from 'react';
import { Text } from 'react-native';
import { global } from '../../Style';
// import { styles } from './text.style';

export const CustomText = props => {
  return <Text {...props} style={[props.style, global.fontFamily]} />;
};
