import React, { PureComponent } from 'react';
import { TouchableOpacity } from 'react-native';
import { Text } from '../';
import { styles } from './button.style';

export class Button extends PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  public render(): JSX.Element {
    const { children, onPress, testID } = this.props;
    return (
      <TouchableOpacity onPress={() => onPress()} style={styles.buttonStyle} testID={testID}>
        <Text style={styles.textStyle}>{children}</Text>
      </TouchableOpacity>
    );
  }
}

export interface IProps {
  onPress: Function;
  children: string;
  testID?: string;
}
export interface IState {}
