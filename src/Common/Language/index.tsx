import React, { Component } from 'react';

export const LanguageContext = React.createContext({
  language: 'en',
  changeLanguage: language => null,
});

interface IProps {}
interface IState {
  language: string;
}

export const LanguageConsumer = LanguageContext.Consumer;

export class LanguageProvider extends Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = { language: 'en' };
  }
  changeLanguage(language) {
    this.setState({
      language,
    });
  }
  render() {
    return (
      <LanguageContext.Provider
        value={{
          language: this.state.language,
          changeLanguage: language => this.changeLanguage(language),
        }}
      >
        {this.props.children}
      </LanguageContext.Provider>
    );
  }
}
