export interface WeatherModel {
  name: string;
  main: WeatherData;
}

interface WeatherData {
  humidity: number;
  pressure: number;
  temp: number;
  temp_max: number;
  temp_min: number;
}
