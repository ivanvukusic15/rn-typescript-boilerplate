export interface UserModel {
  id: number;
  firstName: string;
  lastName: boolean;
  email: string;
}