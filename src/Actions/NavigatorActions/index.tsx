import { createAction } from 'redux-actions';

export namespace NavigatorActions {
  export enum Type {
    INITIALIZE = 'initialize',
  }
  export const initializeNavigator = createAction<string>(NavigatorActions.Type.INITIALIZE);
}
