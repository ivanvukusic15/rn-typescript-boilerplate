import { createAction } from 'redux-actions';
import { UserModel } from '../../Models';

export namespace UserActions {
  export enum Type {
    INITIAL_STATE = 'initial_state',
    USER_SAVE = 'user_save',
  }

  export const updateUser = createAction<UserModel>(UserActions.Type.USER_SAVE);
}
