import fetchMock from 'fetch-mock';
import { WeatherActions } from './';
import { mockStore } from '../../Utils/Mock/mockStore';
import { mockWeather } from '../../Utils/Mock/mockWeather';

describe('async actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('fetching error', () => {
    const url = mockWeather.weatherByCity.url;

    fetchMock.getOnce(url, {
      payload: mockWeather.weatherByCityResponse,
    });

    const expectedActions = {
      payload: mockWeather.weatherByCityResponse,
      type: WeatherActions.Type.WEATHER_UPDATE_SUCCEEDED,
    };

    const store = mockStore();
    return store.dispatch(WeatherActions.currentWeatherByCity(mockWeather.weatherByCity)).then(() => {
      expect(store.getActions()[1]).toEqual(expectedActions);
    });
  });

  it('fetching weather error', () => {
    const url = 'dummy';

    fetchMock.getOnce(url, {
      payload: null,
    });

    const expectedData = {
      type: WeatherActions.Type.WEATHER_UPDATE_SUCCEEDED,
      payload: { error: true, message: WeatherActions.Errors.WEATHER_UPDATE_ERROR },
    };

    const store = mockStore();
    return store.dispatch(WeatherActions.currentWeatherByCity({ url: 'dummy_url', test: true })).then(data => {
      const actionData = {
        type: WeatherActions.Type.WEATHER_UPDATE_SUCCEEDED,
        payload: data,
      };
      expect(actionData).toEqual(expectedData);
    });
  });
});
