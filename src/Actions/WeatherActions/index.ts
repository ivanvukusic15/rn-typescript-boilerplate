import { mockWeather } from '../../Utils/Mock/mockWeather';
import { createActionThunk } from 'redux-thunk-actions';
import { WeatherModel } from '../../Models';

const currentWeatherByCityFunction = async ({ url, test }) => {
  try {
    const response = await fetch(url);
    const responseJson = await response.json();
    if (test) {
      return mockWeather.weatherByCityResponse;
    } else {
      return responseJson;
    }
  } catch (error) {
    return { error: true, message: WeatherActions.Errors.WEATHER_UPDATE_ERROR };
  }
};

export namespace WeatherActions {
  export enum Type {
    WEATHER_UPDATE = 'weather_update',
    WEATHER_UPDATE_SUCCEEDED = 'weather_update_SUCCEEDED',
  }

  export const Errors = {
    WEATHER_UPDATE_ERROR: 'Something went wrong',
  };

  export const currentWeatherByCity: Function = createActionThunk(
    WeatherActions.Type.WEATHER_UPDATE,
    async ({ url, test }) => await currentWeatherByCityFunction({ url, test }),
  );
}
