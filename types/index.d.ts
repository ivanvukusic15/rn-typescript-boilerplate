declare module 'moment';
declare module 'redux';
declare module 'react-native-exception-handler';
declare module 'react-native-restart';
declare module 'react-navigation';
declare module 'react-navigation-redux-helpers';
declare module 'react-redux';
declare module 'redux-thunk';

type Diff<T extends string, U extends string> = ({ [P in T]: P } & { [P in U]: never } & { [x: string]: never })[T];
type Omit<T, K extends keyof T> = { [P in Diff<keyof T, K>]: T[P] };

type PartialPick<T, K extends keyof T> = Partial<T> & Pick<T, K>;
