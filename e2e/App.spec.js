describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('button exists weather', async () => {
    await expect(element(by.id('weatherButton'))).toBeVisible();
    await element(by.id('weatherButton')).tap();
  });

  it('button exists login', async () => {
    await expect(element(by.id('loginButton'))).toBeVisible();
    await element(by.id('loginButton')).tap();
  });
});
